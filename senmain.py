# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys

import chara
import screen
import masu
import gamemain
import AI
import gameon

pygame.init()
pygame.display.set_caption("senryaku")

# color
red = 0
blue = 1
yellow = 2

# formation
attack = 0
defence = 1
balance = 2

#skill
power = 0
block = 1
sonic = 2

#weapon
sword = [0,1]
spear = [0,2]
bow = [1,3]


ocean = [0,0]
weed = [1,1]
ford = [2,2]
terrain = [ocean,weed,ford]

#font
sysfont1 = pygame.font.SysFont(None,40)
sysfont2 = pygame.font.SysFont(None,100)
sysfont3 = pygame.font.SysFont(None,30)

move = sysfont1.render("move",True,(255,255,255))
attack = sysfont1.render("attack",True,(255,255,255))
skill = sysfont1.render("skill",True,(255,255,255))
end = sysfont1.render("end",True,(255,255,255))

tend = sysfont1.render("turn end",True,(255,255,255))
tback = sysfont1.render("title back",True,(255,255,255))

#statuslist
stehp = sysfont3.render("hp:",True,(255,255,255))
steat = sysfont3.render("atk:",True,(255,255,255))
stedf = sysfont3.render("def:",True,(255,255,255))
steweapon = sysfont3.render("weapon:",True,(255,255,255))
stemove = sysfont3.render("move:",True,(255,255,255))
stelist = [stehp,steat,stedf,steweapon,stemove]

titlef = sysfont2.render("This is title",True,(255,255,255))
win = sysfont2.render("You Win",True,(255,255,255))
lose = sysfont2.render("You Lose",True,(255,255,255))

titlelist = [titlef,win,lose]

stage1 = sysfont1.render("stage1",True,(255,255,255))
stage2 = sysfont1.render("stage2",True,(255,255,255))

stagelist = [stage1,stage2]

#fund
cmdlist = [[move,attack,skill,end],[tend,tback]]

#Title
Title = gameon.Title(titlelist,stagelist)

######stage1######  
#chara
chara1_0_0 = chara.character(red,1,1,[15,15],3,3,3,sword,[0,0],power)
chara1_0_1 = chara.character(yellow,2,7,[12,12],3,3,3,spear,[0,1],block)

chara1_1_0 = AI.AI(blue,7,4,[10,10],3,3,2,bow,[1,0],sonic,10)
chara1_1_1 = AI.AI(blue,8,6,[10,10],3,3,3,spear,[1,1],power,0)

charalist1 = [[chara1_0_0,chara1_0_1],[chara1_1_0,chara1_1_1]]

#map
groundmap1 = [[0,0,0,0,0,0,0,0,0,0],
              [0,1,1,1,0,1,1,1,1,0],
              [0,1,1,1,1,2,1,1,2,0],
              [0,1,1,2,1,1,1,0,1,0],
              [0,1,1,0,0,1,1,1,1,0],
              [0,1,1,1,1,1,1,1,0,0],
              [0,1,1,2,0,0,0,1,1,0],
              [0,1,1,1,1,1,1,1,1,0],
              [0,1,2,1,1,0,1,0,1,0],
              [0,0,0,0,0,0,0,0,0,0],]

groundlist1 = masu.groundlist(terrain,groundmap1)
charamap1 = masu.charamap(groundmap1,charalist1)

#masu
masulist1 = masu.mlist(charamap1,groundlist1,charalist1)

#playerx
player1_0 = chara.player(0,masulist1)
player1_1 = chara.player(1,masulist1)

playerlist1 = [player1_0,player1_1]

#class screen
cscreen1 = screen.screen(masulist1,charalist1)


#gamemain   mscreen以外 
gamemain1 = [groundmap1,playerlist1,cscreen1,masulist1,charalist1,charamap1,1]


#####stage2#####
#chara
#my
chara2_0_0 = chara.character(red,3,2,[15,15],5,4,4,sword,[0,0],power)
chara2_0_1 = chara.character(yellow,2,11,[12,12],4,4,3,spear,[0,1],block)
chara2_0_2 = chara.character(red,2,5,[10,10],3,2,2,bow,[0,2],power)
chara2_0_3 = chara.character(yellow,4,7,[15,15],5,5,4,sword,[0,3],block)
chara2_0_4 = chara.character(red,2,2,[12,12],4,3,3,spear,[0,4],power)
chara2_0_5 = chara.character(yellow,3,12,[10,10],3,2,2,bow,[0,5],block)
chara2_0_6 = chara.character(red,1,10,[15,15],4,5,4,sword,[0,6],power)
chara2_0_7 = chara.character(yellow,3,15,[12,12],4,4,3,spear,[0,7],block)

#you
chara2_1_0 = AI.AI(blue,16,3,[10,10],3,3,2,bow,[1,0],sonic,0)
chara2_1_1 = AI.AI(blue,16,5,[10,10],3,3,2,bow,[1,1],sonic,0)
chara2_1_2 = AI.AI(blue,17,9,[10,10],3,3,2,bow,[1,2],sonic,0)
chara2_1_3 = AI.AI(blue,16,14,[10,10],3,3,2,bow,[1,3],sonic,0)
chara2_1_4 = AI.AI(blue,25,3,[10,10],3,3,2,bow,[1,4],sonic,0)
chara2_1_5 = AI.AI(blue,22,5,[10,10],3,3,2,bow,[1,5],sonic,0)
chara2_1_6 = AI.AI(blue,24,14,[10,10],3,3,2,bow,[1,6],sonic,0)
chara2_1_7 = AI.AI(blue,27,8,[10,10],3,3,2,bow,[1,7],sonic,0)
chara2_1_8 = AI.AI(blue,14,6,[10,10],3,3,3,sword,[1,8],sonic,20)
chara2_1_9 = AI.AI(blue,13,13,[10,10],3,3,3,sword,[1,9],sonic,20)
chara2_1_10 = AI.AI(blue,26,4,[10,10],3,3,3,sword,[1,10],sonic,20)
chara2_1_11 = AI.AI(blue,23,13,[10,10],3,3,3,sword,[1,11],sonic,20)
chara2_1_12 = AI.AI(blue,15,7,[10,10],3,3,3,spear,[1,12],sonic,20)
chara2_1_13 = AI.AI(blue,14,14,[10,10],3,3,3,spear,[1,13],sonic,20)
chara2_1_14 = AI.AI(blue,27,6,[10,10],3,3,3,spear,[1,14],sonic,20)
chara2_1_15 = AI.AI(blue,26,10,[10,10],3,3,3,spear,[1,15],sonic,20)

charalist2 = [[chara2_0_0,chara2_0_1,chara2_0_2,chara2_0_3,chara2_0_4,chara2_0_5,chara2_0_6,chara2_0_7],[chara2_1_0,chara2_1_1,chara2_1_2,chara2_1_3,chara2_1_4,chara2_1_5,chara2_1_6,chara2_1_7,chara2_1_8,chara2_1_9,chara2_1_10,chara2_1_11,chara2_1_12,chara2_1_13,chara2_1_14,chara2_1_15]]

#map
groundmap2 = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
              [0,1,1,1,0,1,1,0,1,1,1,2,1,0,0,1,1,0,0,0,1,1,0,0,1,1,2,1,1,0],
              [0,1,1,1,1,2,2,0,1,0,0,0,2,1,1,1,1,0,2,1,1,1,0,0,1,1,2,1,1,0],
              [0,1,1,2,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,2,1,1,0,1,1,0],
              [0,1,1,0,0,1,1,1,2,1,0,1,1,1,1,1,1,1,0,1,0,0,1,0,0,1,1,1,1,0],
              [0,1,1,2,0,0,1,0,1,1,2,1,0,1,0,1,1,1,0,1,1,1,2,1,1,1,1,1,1,0],
              [0,1,1,1,1,1,1,0,1,0,0,1,0,1,1,1,1,1,0,1,1,0,1,0,0,0,0,1,1,0],
              [0,1,2,1,1,0,1,0,1,1,1,1,1,1,0,1,1,0,0,1,1,0,1,0,0,0,0,1,1,0],
              [0,0,1,0,0,0,1,2,1,1,0,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,2,0],
              [0,0,1,0,0,0,1,0,1,1,0,1,0,1,1,1,1,2,1,1,1,1,1,1,1,1,0,1,1,0],
              [0,1,2,1,0,1,1,2,1,1,1,2,1,1,1,1,0,0,0,2,0,1,1,1,1,1,2,1,1,0],
              [0,1,1,1,1,1,1,0,1,1,0,0,1,1,0,1,0,1,0,1,0,1,0,0,0,2,0,1,0,0],
              [0,1,1,2,1,0,1,0,1,1,1,0,1,1,2,1,1,1,0,1,0,1,0,1,1,1,0,1,1,0],
              [0,1,1,0,1,1,1,0,2,0,1,0,0,1,0,1,2,1,0,1,2,1,1,1,1,1,0,1,2,0],
              [0,1,1,1,1,1,1,1,1,0,2,1,1,1,1,2,1,1,0,1,2,1,1,1,1,1,0,1,1,0],
              [0,1,1,2,0,2,1,1,1,1,1,0,1,1,0,1,0,1,2,1,1,1,0,0,0,2,2,1,1,0],
              [0,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,0,1,1,1,2,1,1,1,0,1,1,0],
              [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],]

groundlist2 = masu.groundlist(terrain,groundmap2)
charamap2 = masu.charamap(groundmap2,charalist2)

#masu
masulist2 = masu.mlist(charamap2,groundlist2,charalist2)

#playerx
player2_0 = chara.player(0,masulist2)
player2_1 = chara.player(1,masulist2)

playerlist2 = [player2_0,player2_1]

#class screen
cscreen2 = screen.screen(masulist2,charalist2)


#gamemain
gamemain2 = [groundmap2,playerlist2,cscreen2,masulist2,charalist2,charamap2,1]






