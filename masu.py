# -*- coding: utf-8 -*-
#!/usr/bin/env python

from copy import deepcopy
from Queue import PriorityQueue



def haba(game,lx,ly,idoryoku = None):
    queue = PriorityQueue()
    mapa = []  #移動消費のmap
    movelist = []

    for y in game.masulist:
        mapb = []
        for xy in y:
            if xy.charaID[0] >= 0 and xy.charaID[0] != game.user.ID:
                mapb.append(0)
            else:
                mapb.append(xy.calo)
        mapa.append(mapb)

    mapa[ly][lx] = -1

    charaID = game.masulist[ly][lx].charaID
    chara = game.charalist[charaID[0]][charaID[1]]

    movelist.append([[lx,ly],None,chara.idoryoku])

    if idoryoku == None:
        idoryoku = chara.idoryoku

    queue.put([-idoryoku,lx,ly])
    search = [[1,0],[0,1],[-1,0],[0,-1]]

    while not queue.empty():
        capa,x,y = queue.get()
        capa = - capa
        for dx, dy in search:
            sx = x + dx  #search x
            sy = y + dy  #search y
            if mapa[sy][sx] > 0 and capa >= game.masulist[sy][sx].calo:
                    queue.put([-(capa - game.masulist[sy][sx].calo),sx,sy])
                    mapa[sy][sx] = -1   #can move = -1
                    movelist.append([[sx,sy],[x,y],
                                     capa - game.masulist[sy][sx].calo])
    
    return movelist

#ある範囲指定での幅探
def area(game,lx,ly,area):
    queue = PriorityQueue()
    mapa = []
    arealist = []
    
    for y in game.masulist:
        mapb = []
        for masu in y:
            mapb.append(masu.calo)
        mapa.append(mapb)
    
            
    mapa[ly][lx] = -1
    

    queue.put([-area,lx,ly])
    search = [[0,1],[1,0],[0,-1],[-1,0]]

    while not queue.empty():
        capa,x,y = queue.get()
        capa = - capa
        for dx, dy in search:
            sx = x + dx  #search x
            sy = y + dy  #search y
            if mapa[sy][sx] > 0 and capa >= game.masulist[sy][sx].calo:
                    queue.put([-(capa - game.masulist[sy][sx].calo),sx,sy])
                    mapa[sy][sx] = -1   #can move = -1
                    arealist.append([[sx,sy],[x,y],
                                     capa - game.masulist[sy][sx].calo])
    
    return arealist    

#移動の最後の座標の探索
def move_end(movelist,end):
    endlist = []
    for move in movelist:
        if move[2] == end:
            endlist.append(move[0])
    
    return endlist

#最短経路
def route(habalist,tag,my):  #tag  目標座標    my  自分の座標    
    routelist = []
    result = 0   #経路の途中を一時保存

    #まず幅探したりすとの中からtagと同じ座標のものを捜す
    for i,zahyou in enumerate(habalist):
        if tag == zahyou[0]:
            routelist.append(zahyou)
            result = habalist.pop(i)
            break

    #順番にるーとを作っていく
    while result != None:
        for i,zahyou in enumerate(habalist):
            if result[1] == my:
                routelist.append([my,None,None])
                result = None
                break
            elif result[1] == zahyou[0]:
                routelist.append(zahyou)
                result = habalist.pop(i)
                break

    return routelist
        



#habaで求めたmovelistからきゃらがいる座標を拔く
def canmove(game,movelist):
    canmovelist = []
    for move in movelist:
        if game.masulist[move[0][1]][move[0][0]].charaID[0] == -1:
            canmovelist.append(move)
    
    return canmovelist


def idomap(masulist,movelist):
    mapa = []
    for y in masulist:
        mapb = []
        for x in masulist[0]:
            mapb.append(0)
        mapa.append(mapb)
    for move in movelist:
        mapa[move[0][1]][move[0][0]] = -1

    return mapa

def diamond(maxi,x,y,maps):  #maxi 最大範囲
                             #武器範囲にも使う
                             #くりぬきdiaも

    dia = []
    for i in range(-maxi,maxi + 1):
        for j in range(-abs(-maxi+abs(i)),abs(maxi + 1 - abs(i))):
            if 0 <= y + j <= len(maps) -1:
                if 0 <= x + i <= len(maps[0]) - 1:
                    dia.append([x + i,y + j])
    return dia

    

class masu:
    def __init__(self,masux,masuy,groundlist,charamap,charaID):
        self.masux = masux
        self.masuy = masuy
        self.groundlist = groundlist  #mapchipと消費移動のlist
        self.charamap = charamap
        self.ground = self.groundlist[self.masuy][self.masux][0]    #地形の種類
        self.calo = self.groundlist[self.masuy][self.masux][1]
        self.charaID = self.charamap[self.masuy][self.masux]   #きゃらの初期位置


def charamap(maps,charalist):
    map_c = deepcopy(maps)

    for i,y in enumerate(maps):
        for j,xy in enumerate(y):
            map_c[i][j] = [-1,0]

    for i,y in enumerate(charalist):
        for j,xy in enumerate(y):
            map_c[xy.masuy][xy.masux] = xy.ID
    return map_c


def groundlist(terrain,groundmap):  #intをlistに変更
    for i,y in enumerate(groundmap):
        for j,xy in enumerate(y):
            groundmap[i][j] = terrain[xy]

    return groundmap


def mlist(charamap,groundlist,charalist):  #masulist
    mlist1 = []

    for i,y in enumerate(groundlist):
        mlist2 = []
        for j,xy in enumerate(y):
            mlist2.append(masu(j,i,groundlist,charamap,charalist))
        mlist1.append(mlist2)

    return mlist1


    
