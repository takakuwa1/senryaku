# -*- coding: utf-8 -*-
#!/usr/bin/env python 


import masu

#あにめーしょん
class anime:
    def __init__(self,cycle,routelist,animelist = None):
        self.frame = 0
        self.cycle = cycle   #回す周期
        self.routelist = routelist
        self.animelist = animelist
        self.switch = -1  #ふれーむが動くときの判定
        self.chalist = []  #移動先のきゃらの保持
        self.cID = None #保存したきゃらを一時保存
        self.at_anime = [0,1,-1]

    #移動あにめ
    def move(self,game):
        state = self.frame / self.cycle
        if self.switch != state:
            self.switch = state
            stayed = self.routelist[-(state + 1)] #いた座標
            go = self.routelist[-(state + 2)]   #いく座標

            #いく座標のきゃらを保存
            if game.masulist[go[0][1]][go[0][0]].charaID[0] != -1:
                self.chalist.append(game.masulist[go[0][1]][go[0][0]].charaID)

            chaID = game.masulist[stayed[0][1]][stayed[0][0]].charaID

            #通過したきゃらの復元
            if self.cID != None:
                game.masulist[stayed[0][1]][stayed[0][0]].charaID = self.cID
                self.cID = None
                
            else:
                game.masulist[stayed[0][1]][stayed[0][0]].charaID = [-1,0]


            game.masulist[go[0][1]][go[0][0]].charaID =  chaID
            
            game.charalist[chaID[0]][chaID[1]].masux = go[0][0]
            game.charalist[chaID[0]][chaID[1]].masuy = go[0][1]
            game.charalist[chaID[0]][chaID[1]].lx = game.charalist[chaID[0]][chaID[1]].masux * 30
            game.charalist[chaID[0]][chaID[1]].ly = game.charalist[chaID[0]][chaID[1]].masuy * 30

            #保存したきゃらをchaIDに移す
            if len(self.chalist) > 0:
                self.cID = self.chalist.pop(0)                


            if len(self.routelist) == self.switch + 2:
                game.ani_doing = 0
                if game.ptID != game.AIID:
                    game.cmd.cmd_on = 1
                    game.user.masux = go[0][0]
                    game.user.masuy = go[0][1]

    #攻撃あにめ
    def attack(self,game,god):
        state = self.frame / self.cycle
        if self.switch != state:
            self.switch = state
            atID = game.btID1
            dfID = game.btID2
            at = game.charalist[atID[0]][atID[1]]
            df = game.charalist[dfID[0]][dfID[1]]

            #攻撃側
            if self.switch <= 2:
                dire_x = df.masux - at.masux
                dire_y = df.masuy - at.masuy
                dx = 15 * dire_x / (abs(dire_x) + abs(dire_y))
                dy = 15 * dire_y / (abs(dire_x) + abs(dire_y))
                at.lx += dx * self.at_anime[self.switch]
                at.ly += dy * self.at_anime[self.switch]

            #守備側
            if self.switch % 4 == 0:
                game.charalist[df.ID[0]][df.ID[1]].visible = False
            if self.switch % 4 == 2:
                game.charalist[df.ID[0]][df.ID[1]].visible = True

            #だめーじ計算
            if self.switch == 10:
                damage = 5 + (at.at - df.df)
                df.hp[1] -= damage
                if df.hp[1] <= 0:
                    game.masulist[df.masuy][df.masux].charaID = [-1,0]
                    game.charalist[df.ID[0]][df.ID[1]] = None
                game.ani_doing = 0
                at.esc = 0
                game.win(god,dfID[0])

    #必殺あにめ
    def hissatu(self):
        print "a"
        game.ani_doing = 0
    
    def main(self,game,god):
        self.frame += 1
        if game.ani_doing == "move":
            self.move(game)
            
        elif game.ani_doing == "attack":
            self.attack(game,god)

        elif game.ani_doing == "hissatu":
            self.hissatu()
            
        else:
            game.ani_on = 0
