# -*- coding: utf-8 -*-
#!usr/bin/env python

import pygame
from pygame.locals import *
import sys

import chara
import screen
import masu
import gamemain
import AI
import gameon
import senmain

pygame.init()
pygame.display.set_caption("senryaku")


mscreen = pygame.display.set_mode((900,700)) #main screen
stagelist = [senmain.gamemain1,senmain.gamemain2]

#map
mapchip1 = screen.material(0,0,30,30,"mapchip.png")
mapchip2 = screen.material(100,200,30,30,"mapchip.png")
mapchip3 = screen.material(305,208,30,30,"mapchip.png")

mapchiplist = [mapchip1,mapchip2,mapchip3]

#charaimage
charaimagelist = [screen.material(0,0,30,30,"chara.png"),
                  screen.material(100,0,30,30,"chara.png")]

#command
command = screen.command(senmain.cmdlist)

class god:
    def __init__(self,stagelist,title,mscreen,mapchiplist,charaimagelist,command):
        self.mscreen = mscreen
        self.stagelist = stagelist
        self.si = None   #stage information
        self.stage = None
        self.title = title
        self.state = 0
        self.mapchiplist = mapchiplist
        self.charaimagelist = charaimagelist
        self.command = command 

    def main(self):
        while True:
            pygame.time.Clock().tick(20)
            self.mscreen.fill((0,0,0))

            if self.state == 1:
                self.stage.game(self)

            else:
                self.title.control(self)

                if self.state == 0:
                    self.title.title_draw(self)

                else:
                    self.title.end_draw(self)

            pygame.display.update()



obelisk = god(stagelist,senmain.Title,mscreen,mapchiplist,charaimagelist,command)
obelisk.main()
