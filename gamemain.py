# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys

import screen
import chara
import masu
import controller
import anime
import AI
import senmain

class gamemain: 
    def __init__(self,maps,playerlist,cscreen,masulist,charalist,cmd,charamap,charaimage,mapchip,AIID):
        self.ptID = 0
        self.maps = maps
        self.playerlist = playerlist
        self.cscreen = cscreen
        self.masulist = masulist
        self.charalist = charalist   #charalist[ptID][cID]
        self.user = playerlist[self.ptID]  #活動中のplayer
        self.charaID = self.masulist[self.user.masux][self.user.masuy].charaID
        self.chara = 0   #charaの指定
        self.chara_st = 0  #charaの基準
        self.cmd = cmd   #characommandとplayercommand
        self.cmd_on = self.cmd.cmd_on
        self.turn = self.cmd.turn
        self.atlist = []
        self.natlist = [] #not atlist
        self.movelist = []
        self.charamap = charamap
        self.charaimage = charaimage #charaimagelist
        self.mapchip = mapchip
        self.drawing = 0  #move attack 描写たいみんぐ
        self.menu = 0
        self.anime = None  #あにめくらす  その都度作る
        self.ani_on = 0 #あにめーしょん起動
        self.cycle = None #anime cycle
        self.ani_doing = 0  #一つのあにめーしょん ０終了  else moveとか
        self.create_anime = 0 #anime classを作るかどうか
        self.routelist = []
        self.AIID = AIID
        self.AInum = 0
        self.btID1 = None
        self.btID2 = None
        

    def pturn(self):   #user切り換え playerlistのindex
        self.turn = self.cmd.turn
        self.ptID = self.turn % len(self.playerlist)
        self.user = self.playerlist[self.ptID]
        
    def veri(self):
        if self.masulist[self.user.masuy][self.user.masux].charaID[0] == self.ptID:
            return True

    def charaIDget(self):
        self.charaID = self.masulist[self.user.masuy][self.user.masux].charaID

    def charaget(self):
        if self.charaID[0] >= 0:
            self.chara = self.charalist[self.charaID[0]][self.charaID[1]]

    #勝利条件
    def win(self,god,playerID):
        player = self.charalist[playerID]
        for chara in player:
            if chara != None: return
        if playerID == self.AIID:
            god.state = 2

        else:
            god.state = 3
        self.cmd.turn = 0

    def battle(self,ID1,ID2):
        self.btID1 = ID1
        self.btID2 = ID2
        if ID1[0] != ID2[0]:
            self.ani_on = 1
            self.ani_doing = "attack"
            self.create_anime = 1
            self.cycle = 2

    #きゃらの行動復活
    def reset(self):
        self.pturn()
        player = self.charalist[self.ptID]
        for chara in player:
            if chara != None:
                chara.esc = 2

    def game(self,god):
        if god.state != 1: return
        self.cscreen.map_draw(god)
        self.cscreen.move_draw(god,self.movelist)
        self.cscreen.attack_draw(god,self.atlist,self.natlist)
        self.cscreen.chara_draw(god,self)
        self.cscreen.ID_color_draw(god,self)
        self.cscreen.chara_status_draw(god,self,senmain.sysfont3,senmain.stelist)
        self.cscreen.can_do_draw(god,self)
        self.cscreen.damage_draw(god,self,senmain.sysfont3)
        self.cmd.draw(god)
        self.charaIDget()  #逐一ID変更
        self.pturn()  #player turn の確認

        if self.ani_on == 1:
            if self.create_anime == 1:
                self.anime = anime.anime(self.cycle,
                                         self.routelist)
                self.create_anime = 0
            self.anime.main(self,god)
            pygame.event.get()
        elif self.ptID == self.AIID: 
            AIlist = self.charalist[self.ptID]
            if self.AInum == len(AIlist):
                self.cmd.turn += 1
                self.reset()
                self.AInum = 0
            else:
                AI = AIlist[self.AInum]
                if AI == None:
                    self.AInum += 1
                else:
                    AI.main(self)

        else:
            self.cscreen.cur_draw(god,self.user)
            controller.controller(self,god)
            
        
