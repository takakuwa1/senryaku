#-*- coding: utf-8 -*-
#!/usr/bin/env python

import masu
import chara


class AI(chara.character):

    def __init__(self,color,masux,masuy,hp,at,df,idoryoku,weapon,ID,skill,area,name = 0,stamina = 5):
        chara.character.__init__(self,color,masux,masuy,hp,at,df,idoryoku,weapon,ID,skill,name,stamina)
        self.area = area  #ATT,DEF  索敵範囲
        self.do = None
        self.dire = None
        self.tag = None

    #攻撃可能範囲作成
    def attacklist(self,game,area = None):
        atset = set()
        movelist = masu.haba(game,self.masux,self.masuy,area)
        atlist = []
        for move in movelist:
            dia = masu.diamond(self.weapon[1],move[0][0],move[0][1],game.maps)
                
            for at in dia:
                x = at[0]
                y = at[1]
                atset.add((x,y))
        for at in atset:
            atlist.append(list(at))
        
        return atlist


    #索敵 (攻撃可能)
    def search(self,masulist,atlist):
        searchlist = []
        for attack in atlist:
            masu = masulist[attack[1]][attack[0]]
            charaID = masu.charaID
            if charaID != [-1,0] and charaID[0] != self.ID[0]:
                searchlist.append(attack)
                
        return searchlist


    #行動対象の座標が返り値
    def target(self,game,searchlist,area):  #area  ATTのときは20,DEFのときは0
        damagelist = []  #[座標,きゃら,だめーじ量]のりすと
        result = None  #行動対象の座標  0は攻撃可能,1は接近
        ido = None
        if len(searchlist) != 0:
            #ふたつのりすとを作成
            for x,y in searchlist:
                charaID = game.masulist[y][x].charaID
                chara = game.charalist[charaID[0]][charaID[1]]
                damage = 5 + (self.at - chara.df)
                damagelist.append([[x,y],chara,damage])

            #攻撃優先度
            subresult = 0  #rate結果を一時保存 
            for info in damagelist:
                hp = info[1].hp[1] - info[2]   #残りhp
                rate = float(info[2]) / float(info[1].hp[0])  #だめーじの比率
                
                if hp <= 0:
                    result = [info[0],0]
                    break

                elif rate > subresult:
                    subresult = rate
                    result = [info[0],0]
                
        else:
            targetlist = []  #範囲内の敵座標りすと
            searchlist = masu.area(game,self.masux,self.masuy,
                                   self.area)

            for search in searchlist:
                x = search[0][0]
                y = search[0][1]
                capa = search[2]
                charaID = game.masulist[y][x].charaID
                if charaID[0] >= 0 and self.ID[0] != charaID[0]:
                    targetlist.append([[x,y],capa])


            for target in targetlist:
                if result == None:
                    result = [target[0],1]
                    ido = target[1]   #残りの移動力
                elif ido <= target[1]:
                    result = [target[0],1]
                    ido = target[1]

        return result

    #AIの動き
    def attacking(self,game,tx,ty):
        tagcharaID = game.masulist[ty][tx].charaID
        game.battle(self.ID,tagcharaID)

    #移動先決定
    def direction(self,game,tag):  #tag  行動対象の座標
        if tag[1] == 1:
            searchlist = masu.area(game,self.masux,self.masuy,
                                   self.area)
            routelist = masu.route(searchlist,tag[0],
                                   [self.masux,self.masuy])
            movelist = masu.haba(game,self.masux,self.masuy)

            end = 0
            result = None
            while not result:
                endlist = masu.move_end(movelist,end)
                for route in routelist:
                    if route[0] in endlist:
                        x = route[0][0]
                        y = route[0][1]
                        ma = game.masulist[y][x]
                        charaID = ma.charaID
                        if charaID[0] == -1:
                            result = route[0]
                            break
                end += 1
                
            self.dire = result

            self.do = "move"

        elif tag[1] == 0:
            charaID = game.masulist[tag[0][1]][tag[0][0]].charaID
            chara = game.charalist[charaID[0]][charaID[1]]
            dia1 = masu.diamond(self.weapon[1],tag[0][0],tag[0][1],game.maps)
            dia2 = masu.diamond(self.weapon[0],tag[0][0],tag[0][1],game.maps)
            searchlist = [x for x in dia1 if x not in dia2]

            #移動できる位置のりすとをつくる
            movelist = masu.haba(game,self.masux,self.masuy)
            move = []

            for i in movelist:
                move.append(i[0])

            
            #攻撃できる位置と移動できる位置を導く
            searchlist = [x for x in searchlist if x in move]

            distance = 0
            for search in searchlist:
                x = search[0]
                y = search[1]
                m = game.masulist[y][x]
                if m.charaID[0] < 0 and m.calo > 0:
                    dx = abs(x - tag[0][0])
                    dy = abs(y - tag[0][1])

                    if self.dire == None:
                        self.dire = [x,y]
                        distance = dx + dy
                    elif distance < dx + dy:
                        self.dire = [x,y]
                        distance = dx + dy
                    
            self.do = "move"

    def main(self,game):
        if self.do == None:
            atlist = self.attacklist(game)
            searchlist = self.search(game.masulist,atlist)
            self.tag = self.target(game,searchlist,self.area)
            if self.tag == None:
                self.do = "end"
            else:
                self.direction(game,self.tag)
                
        elif self.do == "move":
            self.move(game,self.dire)
            self.do = "attack"

        elif self.do == "attack":
            if self.tag[1] == 0:
                self.attacking(game,self.tag[0][0],self.tag[0][1])
            self.do = "end"

        elif self.do == "end":
            self.esc = 0
            game.AInum += 1
            self.do = None

