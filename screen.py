#-*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys

import masu

screen_s = pygame.display.set_mode((400,400))

def material(a,b,c,d,filename,colorkey = None):   #素材をつくるもの
    image = pygame.image.load(filename).convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey,RLEACCEL)
    return image.subsurface((a,b,c,d))
    
class screen:
    def __init__(self,masulist,charalist):
        self.masulist = masulist
        self.draw_on = 0  #(1,move)(2,attack)

    def map_draw(self,god):
        for i,y in enumerate(self.masulist):
            for j,xy in enumerate(y):
                god.mscreen.blit(god.mapchiplist[xy.ground],(j*30,i*30))

    def chara_draw(self,god,game):
        for p,playerID in enumerate(game.charalist):
            for chara in playerID:
                if chara != None and chara.visible == True:
                    image = game.charaimage[p]
                    god.mscreen.blit(image,(chara.lx,chara.ly))

    def cur_draw(self,god,user):
        pygame.draw.rect(god.mscreen,(255,255,255),user.rect,5)
        
    def move_draw(self,god,movelist):
        if self.draw_on == 1:
            for move in movelist:
                pygame.draw.rect(god.mscreen,(0,0,255),Rect(1+move[0][0] * 30,1+move[0][1] * 30,28,28))


    def attack_draw(self,god,atlist,natlist):
        if self.draw_on == 2:
            for i,at in enumerate(atlist):
                pygame.draw.rect(god.mscreen,(255,0,0),Rect(1+at[0] * 30,1+at[1] * 30,28,28))
            for j,nat in enumerate(natlist):
                god.mscreen.blit(god.mapchiplist[self.masulist[nat[1]][nat[0]].ground],(nat[0]*30,nat[1]*30))

        
    #すてーたす情報の表示
    def chara_status_draw(self,god,game,font,stelist):
        x = game.user.masux
        y = game.user.masuy
        charaID = game.masulist[y][x].charaID
        chara = game.charalist[charaID[0]][charaID[1]]
        if charaID[0] != -1:
            for i,status in enumerate(chara.statuslist):
                x = 250
                y = 30 * len(game.maps) + 25 * i
                god.mscreen.blit(stelist[i],(x,y))

                if i == 0:
                    status1 = font.render(str(status[1]),True,(255,255,255))
                    status2 = font.render("/",True,(255,255,255))
                    status3 = font.render(str(status[0]),True,(255,255,255))
                    god.mscreen.blit(status1,(x + 40,y))
                    god.mscreen.blit(status2,(x + 70,y))
                    god.mscreen.blit(status3,(x + 85,y))

                elif i == 3:
                    status1 = font.render(str(status[0] + 1),True,(255,255,255))
                    status2 = font.render("~",True,(255,255,255))
                    status3 = font.render(str(status[1]),True,(255,255,255))        
                    god.mscreen.blit(status1,(x + 100,y))
                    god.mscreen.blit(status2,(x + 115,y))
                    god.mscreen.blit(status3,(x + 130,y))                   
                elif i == 4:
                    status1 = font.render(str(status),True,(255,255,255))
                    god.mscreen.blit(status1,(x + 80,y))
 
                else:
                    status1 = font.render(str(status),True,(255,255,255))
                    god.mscreen.blit(status1,(x + 50,y))


            
    def damage_draw(self,god,game,font):
        x = 0
        y = 30 * len(game.maps)
        dmg = font.render("damage:",True,(255,255,255))
        god.mscreen.blit(dmg,(x,y))

        x = 80
        dmg1 = font.render("5",True,(255,255,255))
        dmg2 = font.render("+",True,(255,255,255))
        dmg3 = font.render("(atk - def)",True,(255,255,255))
        god.mscreen.blit(dmg1,(x + 10,y))
        god.mscreen.blit(dmg2,(x + 30,y))
        god.mscreen.blit(dmg3,(x + 50,y))

    def ID_color_draw(self,god,game):
        for player in game.charalist:
            for chara in player:
                if chara != None:
                    x = chara.lx
                    y = chara.ly
                    if chara.ID[0] == 0:
                        pygame.draw.rect(god.mscreen,(255,255,0),Rect(x,y,10,10))
                    elif chara.ID[0] == 1:
                        pygame.draw.rect(god.mscreen,(255,0,255),Rect(x,y,10,10))

    def can_do_draw(self,god,game):
        for player in game.charalist:
            for chara in player:
                if chara != None:
                    x = chara.lx
                    y = chara.ly
                    if chara.esc == 0:
                        pygame.draw.rect(god.mscreen,(128,128,128),Rect(x + 20,y,10,10))
                    elif chara.esc == 1:
                        pygame.draw.rect(god.mscreen,(255,0,0),Rect(x + 20,y,10,10))
                    elif chara.esc == 2:
                        pygame.draw.rect(god.mscreen,(0,0,255),Rect(x + 20,y,10,10))
    

class command():
    def __init__(self,cmdlist):
        self.cmd_on = 0
        self.move = 0
        self.attack = 1
        self.skill = 2
        self.end = 3
        self.tend = 0  #turnend
        self.title = 1  #titleback
        self.crect = None  #chara rect
        self.grect = None  #game rect
        self.inner_crect = None
        self.text_crect = None
        self.inner_grect = None
        self.text_grect = None
        self.cmdlist = cmdlist  #command項目
        self.ccommand = self.move  #characommand
        self.pcommand = self.tend  #playercommand
        self.turn = 0   #playercmdがturn管理

    def make_rect(self,game):
        x = game.user.masux
        y = game.user.masuy

        if game.cmd.cmd_on == 1:

            if x > len(game.maps[0]) / 2:
                dx = -170
                if y > len(game.maps) / 2:
                    dy = -120
                else:
                    dy = -10

            else:
                dx = 50
                if y > len(game.maps) / 2:
                    dy = -120
                else:
                    dy = -10

            self.crect = Rect(x * 30 + dx,y * 30 + dy,150,180)
            self.inner_crect = self.crect.inflate(-10,-10)
            self.text_crect = self.crect.inflate(-30,-30)

        else:
            if x > len(game.maps[0]) / 2:
                dx = -220
                if y > len(game.maps) / 2:
                    dy = -50
                else:
                    dy = -10

            else:
                dx = 50
                if y > len(game.maps) / 2:
                    dy = -50
                else:
                    dy = -10

            self.grect = Rect(x * 30 + dx,y * 30 + dy,200,110)
            self.inner_grect = self.grect.inflate(-10,-10)
            self.text_grect = self.grect.inflate(-30,-30)


    def draw(self,god):
        if self.cmd_on == 1:
            self.make_rect(god.stage)
            pygame.draw.rect(god.mscreen,(255,255,255),self.crect)
            pygame.draw.rect(god.mscreen,(0,0,0),self.inner_crect)
            for i in range(0,4):
                dx = self.text_crect[0] + 30
                dy = self.text_crect[1] + 10 + 40 * i
                god.mscreen.blit(self.cmdlist[0][i],(dx,dy))
            curx = self.text_crect[0]
            cury = self.text_crect[1] + 18 + 40 * self.ccommand
            pygame.draw.rect(god.mscreen,(255,255,255),Rect(curx,cury,10,10))

        if self.cmd_on == 2:
            self.make_rect(god.stage)
            pygame.draw.rect(god.mscreen,(255,255,255),self.grect)
            pygame.draw.rect(god.mscreen,(0,0,0),self.inner_grect)
            for i in range(0,2):
                dx = self.text_grect[0] + 30
                dy = self.text_grect[1] + 10 + 40 * i
                god.mscreen.blit(self.cmdlist[1][i],(dx,dy))
            curx = self.text_grect[0]
            cury = self.text_grect[1] + 18 + 40 * self.pcommand
            pygame.draw.rect(god.mscreen,(255,255,255),Rect(curx,cury,10,10))

def handler(god,game,event):
    if game.cmd.cmd_on == 1:
        if event.type == KEYDOWN and event.key == K_DOWN:
            game.cmd.ccommand = (game.cmd.ccommand + 1) % 4

        if event.type == KEYDOWN and event.key == K_UP:
            game.cmd.ccommand = (game.cmd.ccommand + 3) % 4

        if event.type == KEYDOWN and event.key == K_z:
            if game.cmd.ccommand == game.cmd.move and game.chara.esc == 2:
                game.cscreen.draw_on = 1
                game.user.fix()
                game.drawing = 2

            elif game.cmd.ccommand == game.cmd.attack:
                game.cscreen.draw_on = 2
                game.user.fix()
                game.drawing = 2
                
            elif game.cmd.ccommand == game.cmd.skill:
                game.chara.hissatu(game,1)
        
            elif game.cmd.ccommand == game.cmd.end:
                game.chara.end()
                game.cmd.ccommand = 0
            game.cmd.cmd_on = 0

    if game.cmd.cmd_on == 2:
        if event.type == KEYDOWN and event.key == K_UP:
            if game.cmd.pcommand == 0:
                game.cmd.pcommand = 1
            else:
                game.cmd.pcommand = 0

        if event.type == KEYDOWN and event.key == K_DOWN:
            if game.cmd.pcommand == 1:
                game.cmd.pcommand = 0
            else:
                game.cmd.pcommand = 1

        if event.type == KEYDOWN and event.key == K_z:
            if game.cmd.pcommand == 0:  #turn end
                game.cmd.turn += 1
                game.menu = 1
                game.reset()
            elif game.cmd.pcommand == 1:  #titile
                god.state = 0
                game.cmd.pcommand = 0
            game.cmd.cmd_on = 0
