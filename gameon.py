# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys

from copy import deepcopy

import gamemain

class Title:
    def __init__(self,fontlist,stagelist):
        self.fontlist = fontlist #[title,win,lose] 
        self.stagelist = stagelist
        self.cur = 0

    def title_draw(self,god):
        if god.state != 0: return
        god.mscreen.blit(self.fontlist[0],(200,100))
        for i,stage in enumerate(self.stagelist):
            god.mscreen.blit(stage,(250,200 + i * 30))
        "かーそる"
        curx = 220
        cury = 210 + self.cur * 30
        pygame.draw.rect(god.mscreen,(255,255,255),Rect(curx,cury,10,10))

    def end_draw(self,god):
        if god.state == 0 or god.state == 1: return
        if god.state == 2:
            god.mscreen.blit(self.fontlist[1],(250,200))
        
        if god.state == 3:
            god.mscreen.blit(self.fontlist[2],(250,200))

    def control(self,god):
        if god.state == 1: return
        for event in pygame.event.get():

            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()

            if event.type == KEYDOWN and event.key == K_DOWN:
                self.cur = (self.cur + 1) % len(self.stagelist)

            if event.type == KEYDOWN and event.key == K_UP:
                self.cur = (self.cur - 1) % len(self.stagelist)

            if event.type == KEYDOWN and event.key == K_z:
                if  god.state == 2 or god.state == 3:
                    god.state = 0

                elif god.state == 0:
                    god.state = 1
                    god.si = deepcopy(god.stagelist[self.cur])
                    god.stage = gamemain.gamemain(god.si[0],god.si[1],god.si[2],god.si[3],god.si[4],god.command,god.si[5],god.charaimagelist,god.mapchiplist,god.si[6])
