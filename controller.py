# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame .locals import *
import sys

import screen
import chara
import masu

def controller(game,god):
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            pygame.quit()
            sys.exit()
        game.user.cur_move(game,event,game.cmd.cmd_on)

        if game.cmd.cmd_on == 2:
            screen.handler(god,game,event)

        if game.cmd.cmd_on == 1:
            game.charaget()
            game.user.fix()
            game.atlist = masu.diamond(
                game.chara.weapon[1],
                game.user.masux,
                game.user.masuy,
                game.maps)
            game.natlist = masu.diamond(
                game.chara.weapon[0],
                game.user.masux,
                game.user.masuy,
                game.maps)
            game.movelist = masu.haba(game,
                                      game.user.masux,
                                      game.user.masuy)
                             
            screen.handler(god,game,event)

        if event.type == KEYDOWN and event.key == K_c:
                if game.cscreen.draw_on == 0:
                    game.cmd.cmd_on = 2
                        
        if event.type == KEYDOWN and event.key == K_z:
            game.charaget()
            if game.cscreen.draw_on == 0 and game.veri() and game.chara.esc > 0 and game.menu == 0:  #zが最初に押される
                game.cmd.cmd_on = 1

            if game.drawing == 1:
                #攻撃
                if game.cscreen.draw_on == 2:
                    if game.masulist[game.user.masuy][game.user.masux].charaID[0] >= 0:
                        game.chara_stID = game.masulist[game.user.sty][game.user.stx].charaID   #基準の書き換え
                        game.battle(game.chara_stID,game.charaID)
                  
                #移動
                elif game.cscreen.draw_on == 1: 
                    game.chara.move(game,[game.user.masux,game.user.masuy])

                game.drawing = 0
                game.cscreen.draw_on = 0
                game.cmd.ccommand = 0
                game.cmd.pcommand = 0
                        
            elif game.drawing == 2:
                game.drawing = 1

            elif game.menu == 1:
                game.menu = 0

        if event.type == KEYDOWN and event.key == K_x:
            game.drawing = 0
            game.cmd.cmd_on = 0
            game.cscreen.draw_on = 0
            game.cmd.ccommand = 0
            game.cmd.pcommand = 0
            game.menu = 0
            

