# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys
import math
from copy import deepcopy
import masu

class character(pygame.sprite.Sprite):
    def __init__(self,color,masux,masuy,hp,at,df,idoryoku,weapon,ID,skill,name = 0,stamina = 5): 
        pygame.sprite.Sprite.__init__(self)
        self.masux = masux  #初期位置
        self.masuy = masuy
        self.lx = masux * 30
        self.ly = masuy * 30
        self.ID = ID   #playerIDとcharaIDのりすと
        self.weapon = weapon  #攻撃最大範囲
        self.stamina = stamina
        self.name = name
        self.skill = skill
        self.color = color
        self.idoryoku = idoryoku
        self.hp = hp  #最大体力と現在の体力
        self.at = at
        self.df = df
        self.turn = -1   #すきる
        self.esc = 2   #行動力
        self.visible = True
        self.statuslist = [self.hp,self.at,self.df,self.weapon,self.idoryoku]


    def move(self,game,tag):   #tag 目標地点の座標
        if game.masulist[tag[1]][tag[0]].charaID[0] == -1 and self.esc == 2:
            movelist = masu.haba(game,self.masux,self.masuy)

            for move in movelist:
                if move[0] == tag:
                    game.routelist = masu.route(movelist,tag,[self.masux,self.masuy])
                    game.ani_on = 1
                    game.ani_doing = "move"
                    game.create_anime = 1
                    game.cycle = 4
                    self.esc -= 1

    def hissatu(self,game,skillon):
        if skillon == 1:
            self.turn += 4
            skillon -= 1
            if self.skill == 1:
                self.at += 3
            if self.skill == 2:
                self.df += 3
            if self.skill == 3:
                self.idoryoku += 3
        if self.turn >= 0:
            self.turn -= 1
        if self.turn == 0:
            if skill == 1:
                self.idoryoku -= 3
            if skill == 2:
                self.at -= 3
            if skill == 3:
                self.df -= 3
        game.ani_doing = "hissatu"

    def end(self):
        self.esc = 0

    def affinity(self):
        listbase = [-1,0,1,-1,0]
        return listbase[self.color:self.color + 2]

class player:
    def __init__(self,ID,masulist):
        self.ID = ID 
        self.masux = 0
        self.masuy = 0
        self.lx = 0
        self.ly = 0
        self.rect = Rect(self.lx,self.ly,28,28)
        self.stx = 0
        self.sty = 0

    def fix(self):  #座標の基準
        self.stx = self.masux
        self.sty = self.masuy

    def cur_move(self,game,event,cmd_on):
        if cmd_on == 0:
            if event.type == KEYDOWN and event.key == K_RIGHT:
                if self.masux + 1 < len(game.maps[0]): 
                    self.masux += 1
                    self.lx = self.masux * 30
            if event.type == KEYDOWN and event.key == K_LEFT:
                if self.masux - 1 >= 0:
                    self.masux -= 1
                    self.lx = self.masux * 30 
            if event.type == KEYDOWN and event.key == K_UP:
                if self.masuy - 1 >= 0:
                    self.masuy -= 1
                    self.ly = self.masuy * 30 
            if event.type == KEYDOWN and event.key == K_DOWN:
                if self.masuy + 1 < len(game.maps):
                    self.masuy += 1
                    self.ly = self.masuy * 30
            self.rect = Rect(self.lx,self.ly,28,28)
